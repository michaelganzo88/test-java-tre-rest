package managerappweb.company;

import java.util.List;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CompanyController {
	@GetMapping("/companies")
	public List<Company> getCompanies() {
		return new CompanyMockedDao().findAllCompanies();
	}
}
