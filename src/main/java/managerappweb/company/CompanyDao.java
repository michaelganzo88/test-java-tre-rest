package managerappweb.company;

import java.util.List;

public interface CompanyDao {
	public void insert(Company company);
	public void update(Company company);
	public void delete(String name);
	public Company findByName(String name);
	public List<Company> findAllCompanies();
	public int companyCount();
}
