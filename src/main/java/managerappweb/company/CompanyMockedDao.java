package managerappweb.company;

import java.util.ArrayList;
import java.util.List;

public class CompanyMockedDao implements CompanyDao {
	
	private final String[] companyNames = new String[]{"CARGEAS", "UBI"};
	private Company[] companies = null;
	
	public CompanyMockedDao() {
		if(companies == null) {
			companies = setCompanies();
		}
	}
	
	private Company[] setCompanies() {
		Company[] arCompany = new Company[companyNames.length];
		for (int i=0; i<companyNames.length; i++) {
			 Company company = new Company();
			 company.setId(companyNames[i]);
			 arCompany[i] = company;
		}
		return arCompany;
	}

	@Override
	public void insert(Company company) {}

	@Override
	public void update(Company company) {}

	@Override
	public void delete(String name) {}

	@Override
	public Company findByName(String name) {
		return null;
	}

	@Override
	public List<Company> findAllCompanies() {
		List<Company> companyList = new ArrayList<>();
		for (Company company : companies) {
			companyList.add(company);
		}
		return companyList;
	}

	@Override
	public int companyCount() {
		return findAllCompanies().size();
	}

}
