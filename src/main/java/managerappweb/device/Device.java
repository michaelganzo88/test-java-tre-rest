package managerappweb.device;

import java.util.HashMap;
import java.util.Map;

public class Device implements Comparable<Device> {
	private String id;
	private String label;
	private Map<String, Integer> size = new HashMap<>();
	private boolean isDefault;
	
	public Device(String name, String label, int width, int height, boolean isDefault) {
		this.id = name;
		this.label = label;
		this.size.put("width", width);
		this.size.put("height", height);
		this.isDefault = isDefault;
	}
	
	public String getId() {
		return id;
	}
	public String getLabel() {
		return label;
	}
	public Map<String, Integer> getSize() {
		return size;
	}
	public boolean isDefault() {
		return isDefault;
	}

	@Override
	public int compareTo(Device o) {
		return this.label.compareTo(o.label);
	}
}
