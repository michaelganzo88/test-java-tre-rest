package managerappweb.device;

public class DeviceBuilder {
	private final String id;
	private final String label;
	private int width;
	private int height;
	private boolean isDefault;
	
	public DeviceBuilder(String id, String label) {
		this.id = id;
		this.label = label;
	}
	
	public DeviceBuilder width(int width) {
		this.width = width;
		return this;
	}
	
	public DeviceBuilder height(int height) {
		this.height = height;
		return this;
	}
	
	public DeviceBuilder defaultValue(boolean isDefault) {
		this.isDefault = isDefault;
		return this;
	}
	
	public Device build() {
		return new Device(this.id, this.label, this.width, this.height, this.isDefault);
	}
}
