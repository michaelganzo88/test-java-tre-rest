package managerappweb.device;

import java.util.List;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class DeviceController {
//	@RequestMapping("/devices")
//	public List<Device> getDevices() {
//		return DeviceSingleton.getInstance().getDevices();
//	}
	
	@RequestMapping("/devices")
	public List<Device> getDevices() {
		return DeviceSingleton.getInstance().getDevices();
	}
}
