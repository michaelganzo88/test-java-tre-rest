package managerappweb.device;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

public class DeviceSingleton {
	
	private static DeviceSingleton instance = null;
	
	private List<Device> devices;
	
	private static final Set<Device> deviceSet = populateDeviceSet();
	private static Set<Device> populateDeviceSet() {
		Set<Device> set = new TreeSet<Device>();
		set.add(new DeviceBuilder("iphone-x", "iPhone X").width(360).height(640).defaultValue(false).build());
		set.add(new DeviceBuilder("note8", "Galaxy Note 8").width(360).height(640).defaultValue(false).build());
		set.add(new DeviceBuilder("iphone8", "iPhone 8").width(412).height(732).defaultValue(false).build());
		set.add(new DeviceBuilder("iphone8plus", "iPhone 8 Plus").width(412).height(732).defaultValue(false).build());
		set.add(new DeviceBuilder("iphone5s", "iPhone 5s").width(320).height(568).defaultValue(false).build());
		set.add(new DeviceBuilder("iphone5c", "iPhone 5c").width(375).height(667).defaultValue(true).build());
		set.add(new DeviceBuilder("ipad", "iPad").width(414).height(736).defaultValue(false).build());
		set.add(new DeviceBuilder("iphone4s", "iPhone 4s").width(768).height(1024).defaultValue(false).build());
		set.add(new DeviceBuilder("nexus5", "Nexus 5").width(1024).height(1366).defaultValue(false).build());
		set.add(new DeviceBuilder("lumia920", "Lumia 920").width(1024).height(1366).defaultValue(false).build());
		set.add(new DeviceBuilder("s5", "Samsung S5").width(1024).height(1366).defaultValue(false).build());
		set.add(new DeviceBuilder("htc-one", "HTC One").width(1024).height(1366).defaultValue(false).build());
		set.add(new DeviceBuilder("macbook", "MacBook").width(1024).height(1366).defaultValue(false).build());
		return set;
	}
	
	private DeviceSingleton() {
		this.devices = new ArrayList<Device>(deviceSet);
		Collections.sort(this.devices);
	}
	
	public static DeviceSingleton getInstance() {
		if(instance == null) {
			instance = new DeviceSingleton();
		}
		return instance;
	}
	
	public List<Device> getDevices() {		
		return devices;
	}
}
