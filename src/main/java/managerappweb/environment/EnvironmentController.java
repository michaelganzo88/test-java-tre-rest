package managerappweb.environment;

import java.util.List;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class EnvironmentController {
	@GetMapping("/environments")
	public List<Environment> getCompanies() {
		return new EnvironmentMockedDao().findAllEnvironments();
	}
}
