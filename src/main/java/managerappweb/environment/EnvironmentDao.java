package managerappweb.environment;

import java.util.List;

public interface EnvironmentDao {
	public void insert(Environment company);
	public void update(Environment company);
	public void delete(String name);
	public Environment findByName(String name);
	public List<Environment> findAllEnvironments();
	public int environmentCount();
}
