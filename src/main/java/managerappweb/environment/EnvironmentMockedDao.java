package managerappweb.environment;

import java.util.ArrayList;
import java.util.List;

public class EnvironmentMockedDao implements EnvironmentDao {
	
	private final String[] environmentNames = new String[]{
			"sviluppo",
			"integrazione",
			"test factory",
			"produzione"
	};
	private Environment[] environments = null;
	
	public EnvironmentMockedDao() {
		if(environments == null) {
			environments = setEnvironments();
		}
	}
	
	private Environment[] setEnvironments() {
		Environment[] arEnvironment = new Environment[environmentNames.length];
		for (int i=0; i<environmentNames.length; i++) {
			Environment environment = new Environment();
			environment.setId(environmentNames[i]);
			arEnvironment[i] = environment;
		}
		return arEnvironment;
	}

	@Override
	public void insert(Environment company) {}

	@Override
	public void update(Environment company) {}

	@Override
	public void delete(String name) {}

	@Override
	public Environment findByName(String name) {
		return null;
	}

	@Override
	public List<Environment> findAllEnvironments() {
		List<Environment> companyList = new ArrayList<>();
		for (Environment company : environments) {
			companyList.add(company);
		}
		return companyList;
	}

	@Override
	public int environmentCount() {
		return findAllEnvironments().size();
	}

}
