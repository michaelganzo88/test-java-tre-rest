package managerappweb.iframe;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class IFrameController {
	
	@RequestMapping(value="/iframe", method=RequestMethod.GET)
	String iframe(@RequestParam(value="name", required=true) String name, Model model) {
		//model.addAttribute("name", name);
		return "iframe-" + name;
	}
}