package oldfashionpound;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.ObjectMapper;
import oldfashionpound.bean.DivisionBean;
import oldfashionpound.bean.MultiplicationBean;
import oldfashionpound.bean.OperazioneBean;
import oldfashionpound.bean.PriceBean;
import oldfashionpound.bean.SumBean;
import oldfashionpound.bean.SubtractionBean;
import oldfashionpound.math.IMathOperation;
import oldfashionpound.math.MathOperationFactory;

@RestController
public class Controller {
	
	ObjectMapper objectMapper = new ObjectMapper();
	
	@PostMapping(value="/somma")
	public Result somma(@RequestBody final String body) throws Exception {
		Result result = new Result();
		try {
			result.setResult(getResult(objectMapper.readValue(body, SumBean.class)));
		} catch (Exception e) {
			throw e;
		}
		return result;
	}
	
	@PostMapping(value="/sottrazione")
	public Result sottrazione(@RequestBody final String body) {
		Result result = new Result();
		try {
			result.setResult(getResult(objectMapper.readValue(body, SubtractionBean.class)));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}
	
	@PostMapping(value="/moltiplicazione")
	public Result moltiplicazione(@RequestBody final String body) {
		Result result = new Result();
		try {
			result.setResult(getResult(objectMapper.readValue(body, MultiplicationBean.class)));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}
	
	@PostMapping(value="/divisione")
	public Result divisione(@RequestBody final String body) {
		Result result = new Result();
		try {
			result.setResult(getResult(objectMapper.readValue(body, DivisionBean.class)));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}
	
	private PriceBean getResult(OperazioneBean membriOperazione) throws Exception {
		IMathOperation mathOperation = MathOperationFactory.getMathOperation(membriOperazione);
		Price price = mathOperation.getResultPrice();
		return price.toPriceBean();
	}
}
