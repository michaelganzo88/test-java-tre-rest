package oldfashionpound;

import java.util.regex.Pattern;

import oldfashionpound.bean.PriceBean;
import oldfashionpound.unit.IUnit;
import oldfashionpound.unit.UnitFactory;
import oldfashionpound.unit.UnitPence;
import oldfashionpound.unit.UnitPound;
import oldfashionpound.unit.UnitScellino;
import oldfashionpound.unit.UnitTotalPence;

public class Price {
	private UnitPound pound;
	private UnitScellino scellino;
	private UnitPence pence;
	private UnitTotalPence unitPence;
	private int mod;

	public Price(int pennies) throws Exception {
		this.unitPence = new UnitTotalPence(pennies);
		penceToPrice();
	}

	public Price(String price) throws Exception {
		parsePrice(price);
	}
	
	public Price(PriceBean priceBean) throws Exception {
		this.pence = new UnitPence(priceBean.getPence());
		this.scellino = new UnitScellino(priceBean.getScellino());
		this.pound = new UnitPound(priceBean.getPound());
		setUnitPence(this.pence.getPennies() + this.scellino.getPennies() + this.pound.getPennies());
	}
	
	private void parsePrice(String price) throws Exception {
		String regex = "^(\\d+" + Constants.POUND + ")(\\d+" + Constants.SCELLINO + ")(\\d+" + Constants.PENCE + ")$";
		if(Pattern.matches(regex, price)) {
			String[] psp = price.split("(?<=\\d\\D)");
			int totalPence = 0;
			for(String unitPsp: psp) {
				IUnit unit = readUnit(unitPsp);
				setUnits(unit);
				totalPence += unit.getPennies();
			}
			setUnitPence(totalPence);
		}
	}
	
	private IUnit readUnit(String unitPsp) throws Exception {
		String[] unitAr = unitPsp.split("(?=\\D)");
		return UnitFactory.getUnit(unitAr[1].charAt(0), Integer.parseInt(unitAr[0]));
	}
	
	private void setUnits(IUnit unit) {
		if(unit instanceof UnitPence) {
			this.pence = (UnitPence) unit;
		} else if(unit instanceof UnitScellino) {
			this.scellino = (UnitScellino) unit;
		} else if(unit instanceof UnitPound) {
			this.pound = (UnitPound) unit;
		}
	}
	
	private void setUnitPence(int totalPence) throws Exception {
		this.unitPence = new UnitTotalPence(totalPence);
	}

	private void penceToPrice() throws Exception {
		int sMultiplicator = new UnitScellino(0).getMultiplicator();
		int pMultiplicator = new UnitPound(0).getMultiplicator();
		this.pound = new UnitPound(this.unitPence.getPennies() / pMultiplicator);
		this.scellino = new UnitScellino(this.unitPence.getPennies() % pMultiplicator/sMultiplicator);
		this.pence = new UnitPence(this.unitPence.getPennies() % sMultiplicator);
	}

	public int getPennies() {
		return this.unitPence.getPennies();
	}

	public void setMod(int mod) {
		this.mod = mod;
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(pound.getValue()).append(String.valueOf(Constants.POUND)).append(" ")
		.append(scellino.getValue()).append(String.valueOf(Constants.SCELLINO)).append(" ")
		.append(pence.getValue()).append(String.valueOf(Constants.PENCE))
		.append(mod > 0 ? " - " + mod + Constants.MOD : "")
		;
		return sb.toString();
	}
	
	public PriceBean toPriceBean() {
		PriceBean priceBean = new PriceBean();
		priceBean.setPence(this.pence.getValue());
		priceBean.setScellino(this.scellino.getValue());
		priceBean.setPound(this.pound.getValue());
		priceBean.setMod(this.mod);
		return priceBean;
	}
}
