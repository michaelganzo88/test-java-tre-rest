package oldfashionpound;

import oldfashionpound.bean.PriceBean;

public class Result {
	private PriceBean result;

	public PriceBean getResult() {
		return result;
	}

	public void setResult(PriceBean priceBean) {
		this.result = priceBean;
	}
}
