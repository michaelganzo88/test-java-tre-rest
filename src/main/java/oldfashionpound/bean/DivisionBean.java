package oldfashionpound.bean;

public class DivisionBean extends OperazioneBean {
	private int secondValue;

	public int getSecondValue() {
		return secondValue;
	}

	public void setSecondValue(int secondValue) {
		this.secondValue = secondValue;
	}
}
