package oldfashionpound.bean;

public class MultiplicationBean extends OperazioneBean {
	private int secondValue;

	public int getSecondValue() {
		return secondValue;
	}

	public void setSecondValue(int secondValue) {
		this.secondValue = secondValue;
	}
}
