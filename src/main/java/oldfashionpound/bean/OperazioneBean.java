package oldfashionpound.bean;

public abstract class OperazioneBean {
	private PriceBean firstValue;

	public PriceBean getFirstValue() {
		return firstValue;
	}

	public void setFirstValue(PriceBean firstValue) {
		this.firstValue = firstValue;
	}
}
