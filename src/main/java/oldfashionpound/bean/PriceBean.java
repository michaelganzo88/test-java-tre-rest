package oldfashionpound.bean;

public class PriceBean {
	private int pence;
	private int scellino;
	private int pound;
	private int mod;
	
	public int getPence() {
		return pence;
	}
	public void setPence(int pence) {
		this.pence = pence;
	}
	public int getScellino() {
		return scellino;
	}
	public void setScellino(int scellino) {
		this.scellino = scellino;
	}
	public int getPound() {
		return pound;
	}
	public void setPound(int pound) {
		this.pound = pound;
	}
	public int getMod() {
		return mod;
	}
	public void setMod(int mod) {
		this.mod = mod;
	}
}
