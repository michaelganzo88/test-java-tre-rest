package oldfashionpound.bean;

public class SubtractionBean extends OperazioneBean {
	private PriceBean secondValue;
	
	public PriceBean getSecondValue() {
		return secondValue;
	}
	public void setSecondValue(PriceBean secondValue) {
		this.secondValue = secondValue;
	}
}
