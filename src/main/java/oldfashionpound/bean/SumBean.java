package oldfashionpound.bean;

public class SumBean extends OperazioneBean {
	private PriceBean secondValue;
	
	public PriceBean getSecondValue() {
		return secondValue;
	}
	public void setSecondValue(PriceBean secondValue) {
		this.secondValue = secondValue;
	}
}
