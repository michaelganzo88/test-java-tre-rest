package oldfashionpound.math;

import oldfashionpound.Price;

public interface IMathOperation {
	int getResult();
	Price getResultPrice() throws Exception;
}
