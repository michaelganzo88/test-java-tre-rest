package oldfashionpound.math;

import oldfashionpound.Price;

public abstract class MathOperation implements IMathOperation {
	protected int risultato;
	protected int firstArg;
	protected int secondArg;

	protected MathOperation(Price firstArg) {
		this.firstArg = firstArg.getPennies();
	}

	abstract void calculate();

	public int getResult() {
		return risultato;
	}

	public Price getResultPrice() throws Exception {
		return new Price(risultato);
	}
}
