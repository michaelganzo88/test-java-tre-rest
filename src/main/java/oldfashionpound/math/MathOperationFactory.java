package oldfashionpound.math;

import oldfashionpound.Price;
import oldfashionpound.bean.DivisionBean;
import oldfashionpound.bean.MultiplicationBean;
import oldfashionpound.bean.OperazioneBean;
import oldfashionpound.bean.SumBean;
import oldfashionpound.bean.SubtractionBean;

public class MathOperationFactory {

	public static IMathOperation getMathOperation(OperazioneBean operazione) throws Exception {
		IMathOperation mathOperation = null;
		Price firstArg;
		if(operazione instanceof SumBean) {
			SumBean bean = (SumBean) operazione;
			firstArg = new Price(bean.getFirstValue());
			mathOperation = new Sum(firstArg, new Price(bean.getSecondValue()));
		} else if(operazione instanceof SubtractionBean) {
			SubtractionBean bean = (SubtractionBean) operazione;
			firstArg = new Price(bean.getFirstValue());
			mathOperation = new Subtraction(firstArg, new Price(bean.getSecondValue()));
		} else if(operazione instanceof MultiplicationBean) {
			MultiplicationBean bean = (MultiplicationBean) operazione;
			firstArg = new Price(bean.getFirstValue());
			mathOperation = new Multiplication(firstArg, bean.getSecondValue());
		} else if(operazione instanceof DivisionBean) {
			DivisionBean bean = (DivisionBean) operazione;
			firstArg = new Price(bean.getFirstValue());
			mathOperation = new Division(firstArg, bean.getSecondValue());
		}
		return mathOperation;
	}

}
