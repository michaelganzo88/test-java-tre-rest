package oldfashionpound.math;

import oldfashionpound.Price;

public class Subtraction extends Sum {

	public Subtraction(Price firstArg, Price secondArg) {
		super(firstArg, secondArg);
	}

	@Override
	void calculate() {
		this.risultato = this.firstArg - this.secondArg;
	}

}
