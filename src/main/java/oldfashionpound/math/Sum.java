package oldfashionpound.math;

import oldfashionpound.Price;

public class Sum extends MathOperation {

	public Sum(Price firstArg, Price secondArg) {
		super(firstArg);
		this.secondArg = secondArg.getPennies();
		calculate();
	}

	@Override
	void calculate() {
		this.risultato = this.firstArg + this.secondArg;
	}

}
