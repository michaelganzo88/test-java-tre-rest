package oldfashionpound.unit;

public interface IUnit {
	int getValue();
	int getPennies();
}
