package oldfashionpound.unit;

public abstract class Unit implements IUnit {
	protected char identifier;
	protected int multiplicator;
	protected int pennies;
	protected int maxValue;
	protected int value;
	
	public Unit(int value) throws Exception {
		setIdentifier();
		setMultiplicator();
		setMaxValue();
		setPennies(value);
		this.value = value;
		if(value > this.maxValue) {
			throw new Exception("Value too high!");
		}
	}
	
	abstract void setIdentifier();
	abstract void setMultiplicator();
	abstract void setMaxValue();
	
	private void setPennies(int value) {
		this.pennies = value * this.multiplicator;
	}

	public char getIdentifier() {
		return identifier;
	}
	
	public int getMultiplicator() {
		return multiplicator;
	}
	
	public int getValue() {
		return value;
	}
	
	public int getPennies() {
		return pennies;
	}
	
	public int getMaxValue() {
		return maxValue;
	}
}
