package oldfashionpound.unit;

import oldfashionpound.Constants;

public class UnitFactory {

	private UnitFactory() {}
	
	public static IUnit getUnit(char unitIdentifier, int value) throws Exception {
		IUnit unit = null;
		switch (unitIdentifier) {
		case Constants.PENCE:
			unit = new UnitPence(value);
			break;
		case Constants.SCELLINO:
			unit = new UnitScellino(value);
			break;
		case Constants.POUND:
			unit = new UnitPound(value);
			break;
		default:
			break;
		}
		return unit;
	}

}
