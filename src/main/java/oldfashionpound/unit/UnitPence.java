package oldfashionpound.unit;

import oldfashionpound.Constants;

public class UnitPence extends Unit {
	
	public UnitPence(int value) throws Exception {
		super(value);
	}
	
	@Override
	void setIdentifier() {
		this.identifier = Constants.PENCE;
	}
	
	@Override
	void setMultiplicator() {
		this.multiplicator = Constants.dMultiplicationFactor;
	}

	@Override
	void setMaxValue() {
		this.maxValue = 12;
	}

}
