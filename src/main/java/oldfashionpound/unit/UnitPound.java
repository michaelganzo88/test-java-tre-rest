package oldfashionpound.unit;

import oldfashionpound.Constants;

public class UnitPound extends Unit {

	public UnitPound(int value) throws Exception {
		super(value);
	}
	
	@Override
	void setIdentifier() {
		this.identifier = Constants.POUND;
	}
	
	@Override
	void setMultiplicator() {
		this.multiplicator = Constants.pMultiplicationFactor * Constants.sMultiplicationFactor;
	}

	@Override
	void setMaxValue() {
		this.maxValue = Integer.MAX_VALUE;
	}

}
