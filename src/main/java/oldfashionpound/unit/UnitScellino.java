package oldfashionpound.unit;

import oldfashionpound.Constants;

public class UnitScellino extends Unit {
	
	public UnitScellino(int value) throws Exception {
		super(value);
	}

	@Override
	void setIdentifier() {
		this.identifier = Constants.SCELLINO;
	}
	
	@Override
	void setMultiplicator() {
		this.multiplicator = Constants.sMultiplicationFactor * Constants.dMultiplicationFactor;
	}

	@Override
	void setMaxValue() {
		this.maxValue = 20;
	}

}
