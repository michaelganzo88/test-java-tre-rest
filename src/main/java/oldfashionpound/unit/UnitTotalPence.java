package oldfashionpound.unit;

public class UnitTotalPence extends UnitPence {

	public UnitTotalPence(int value) throws Exception {
		super(value);
	}
	
	@Override
	void setMaxValue() {
		this.maxValue = Integer.MAX_VALUE;
	}

}
